const sass = require('node-sass');

 

module.exports = function(grunt){

    grunt.initConfig({

        pkg: grunt.file.readJSON('package.json'),

        sass:{

            options: {

                implementation: sass,

                sourceMap: true

            },

            dist:{

                options: {

                    style: 'expanded'

                },

                files: {

                    './src/css/main.css':'../build/sass/main'

                }

            },

            dev: {

                options: {

                    style: 'expanded'

                },

                files: {

                    './src/css/main.css':'./build/sass/main.scss'

                }

            }

        },

        cssmin: {

            css: {

                src: './src/css/main.css',

                dest: './src/css/main.min.css'

            }

        }

 

    });

 

    grunt.loadNpmTasks('grunt-sass');

    grunt.loadNpmTasks('grunt-contrib-cssmin');

    grunt.loadNpmTasks('grunt-contrib-uglify');

 

    grunt.registerTask('default', ['sass', 'cssmin', 'uglify']);

};