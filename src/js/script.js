const form = document.querySelector(".top-section form");
const input = document.querySelector(".top-section input");
const msg = document.querySelector(".top-section .msg");
const list = document.querySelector(".content-section .cities");

const apiKey = "6c9a19201d52c7ae61c2f8058b987f80";

form.addEventListener("submit", e => {
e.preventDefault();
let inputVal = input.value;

//check if there's already a city
const listItems = list.querySelectorAll(".content-section .city");
const listItemsArray = Array.from(listItems);

if (listItemsArray.length > 0) {
    const filteredArray = listItemsArray.filter(el => {
    let content = "";
    
    if (inputVal.includes(",")) {
        if (inputVal.split(",")[1].length > 2) {
        inputVal = inputVal.split(",")[0];
        content = el
            .querySelector(".city-name span")
            .textContent.toLowerCase();
        } else {
        content = el.querySelector(".city-name").dataset.name.toLowerCase();
        }
    } else {
        content = el.querySelector(".city-name span").textContent.toLowerCase();
    }
        return content == inputVal.toLowerCase();
    });

    if (filteredArray.length > 0) {
        form.reset();
        input.focus();
        
        return;
    }
}

//ajax
const url = `https://api.openweathermap.org/data/2.5/weather?q=${inputVal}&appid=${apiKey}&units=metric`;

fetch(url)
    .then(response => response.json())
    .then(data => {
        const { main, name, sys, weather } = data;
        const icon = `https://s3-us-west-2.amazonaws.com/s.cdpn.io/162656/${weather[0]["icon"]}.svg`;

        const li = document.createElement("li");
        li.classList.add("city");
        const card = `
            <h2 class="city-name" data-name="${name},${sys.country}">
            <span>${name}</span>
            <sup>${sys.country}</sup>
            </h2>
            <div class="city-temp">${Math.round(main.temp)}<sup>&#8451;</sup></div>
            <figure>
            <img class="city-icon" src="${icon}" alt="${
            weather[0]["description"]
        }">
            <figcaption>${weather[0]["description"]}</figcaption>
            </figure>
        `;

        li.innerHTML = card;
        list.insertBefore(li, list.firstChild);
    })
    .catch(() => {
        msg.textContent = "Please search for a valid city";
    });

    msg.textContent = "";
    form.reset();
    input.focus();
});